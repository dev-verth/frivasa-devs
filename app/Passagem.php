<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Passagem extends Model
{
    public $timestamps = false;

    protected $fillable = [
    'id',
    'colaborador_id', 
    'portal_id',
    'cod_situacao', 
    'turno',
    'time_passagem', 
    'porta', 
];

protected $table = 'passagems';
}