<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;



class PassagemFrivasa implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $col;
    public $portal;
    // public $timep;
    // public $porta;
    

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($col,$portal)
    {
        // error_log('Construct passagem frivasa');
        // echo "<br> Construct passagem frivasa</br>";
        $this->col_id = $col;
        $this->portal = $portal;
        // $this->timep = $timep;
        // $this->porta = $porta;
        return response()->json(['col_id' => $col,'portal' => $portal]);
    }

   
    public function broadcastOn()
  {
    return new Channel('passagemchannel');
  }

  public function broadcastAs()
  {
return 'PassagemFrivasa';
  }

 
  
 
}
