<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setor extends Model
{
    protected $primaryKey = 'codigo';
    public $timestamps = false;
    protected $fillable = [
    	'codigo',
    	'nome', 
    ];

    protected $table = 'setors';
}