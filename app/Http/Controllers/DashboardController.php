<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Colaborador;
use App\Setor;
use App\Turno;
use App\Passagem;
use App\Portal;

class DashboardController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }
    public function versionone()
    {
        $lista_colaboradores = Colaborador::all();
        $lista_passagem = Passagem::all();
        $lista_portal = Portal::all();
        $lista_setor = Setor::all();
       // $total=$colaboradors->count();
        //dd($total);

        return view('dashboard.v1', ['colaboradors' => $lista_colaboradores,'passagens' => $lista_passagem,'portais' => $lista_portal,'setores' => $lista_setor]);
    }
    public function versiontwo()
    {
        return view('dashboard.v2');
    }
    public function versionthree()
    {
        return view('dashboard.v3');
    }
}
