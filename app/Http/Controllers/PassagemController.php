<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Passagem;
use App\Colaborador;
use App\Portal;
use App\Events\PassagemFrivasa;
use DB;

class PassagemController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function index()
    {
    	$lista_passagem = Passagem::all();

        return view('passagem.index', ['Passagems' => $lista_passagem]);
        // return view('passagem.teste', ['Passagems' => $lista_passagem]);

    }
   
    // CREATE   

    public function create(){

        $colaboradores = Colaborador::all();
        $portais = Portal::all();

        return view('passagem.create', ['colaboradores' => $colaboradores,'portais' => $portais]);
    }

    public function store(Request $request){
        $passagem = new Passagem();
        $passagem->colaborador_id = $request->colaborador_id;
        $passagem->portal_id = $request->portal_id;
        $passagem->cod_situacao = $request->cod_situacao;
    	$passagem->turno = $request->turno;
    	$passagem->time_passagem = $request->time_passagem;
        $passagem->porta = $request->porta;
        $passagem->save();
    	return redirect('/passagem')->with('message', 'passagem registrado com sucesso!');

    }
    // END CREATE

 
    
    // UPDATE
    public function updateView($passagem_id){
    	$passagem = Passagem::find($passagem_id);
    	return view('passagem.update', ['passagem' => $passagem]);
    }

    public function update(Request $request){
        $passagem = Passagem::findOrFail($request->id);
        $passagem->colaborador_id = $request->colaborador_id;
        $passagem->portal_id = $request->portal_id;
        $passagem->cod_situacao = $request->cod_situacao;
        $passagem->turno = $request->turno;
        $passagem->time_passagem = $request->time_passagem;
        $passagem->porta = $request->porta;

        $passagem->save();

		return redirect("/passagem")->with("message", "passagem atualizado com sucesso!");
    }
    // END UPDATE

    // DETAILED
    public function detailed($passagem_id){
        $passagem = Passagem::find($passagem_id);

        return view('passagem.detailed', ['passagem' => $passagem]);
    }
    // END DETAILED

    public function destroy($id)
    {
        $passagem = passagem::findOrFail($id);
        $passagem->delete();
        return redirect()->route('passagem.index')->with('alert-success','passagem deletado!');
    }
}
