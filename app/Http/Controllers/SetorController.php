<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\setor;
use DB;
class SetorController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function index()
    {       
        $lista_setor = DB::table('setors')->get();
    	return view('setor.index', ['setors' => $lista_setor]);
    }

   
    // CREATE
   

    public function create(){
        return view('setor.create');

    }

    public function store(Request $request){
        $setor = new setor();
        $setor->codigo = $request->codigo;
        $setor->nome = $request->nome;
        $setor->save();
    
    	return redirect('/setor')->with('message', 'setor registrado com sucesso!');
    	
    }
    // END CREATE

    
    // UPDATE
    public function updateView($setor_id){
        
        $setor = Setor::find($setor_id);
        
        $cod_setor = DB::table('setors')->get()->where('codigo', $setor_id)->first();
        
    	return view('setor.update', ['setor' => $setor, 'cod_setor' => $cod_setor]);
    }

    public function update(Request $request){
    	DB::table('setors')->where('codigo', $request->codigo)->update(['nome' => $request->nome,]);

		return redirect("/setor")->with("message", "setor atualizado com sucesso!");
    }
    // END UPDATE

  
    // DETAILED
    public function detailed($setor_id){
        $setor = setor::find($setor_id);
        return view('setor.detailed', ['setor' => $setor]);
    }
    // END DETAILED

    
    public function destroy($codigo)
    {
        $setor = setor::findOrFail($codigo);
        $setor->delete();
        return redirect()->route('setor.index')->with('alert-success','setor deletado!');
    }
}
