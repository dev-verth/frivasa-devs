<?php
namespace App\Http\Controllers;
//require_once 'dompdf/lib/html5lib/Parser.php';
//require_once 'dompdf/lib/php-font-lib-master/src/FontLib/Autoloader.php';
//require_once 'dompdf/lib/php-svg-lib-master/src/autoload.php';
//require_once 'dompdf/src/Autoloader.php';
// definindo os namespaces
//Dompdf\Autoloader::register();
//use Dompdf\Dompdf;
use Illuminate\Http\Request;
use App\Passagem;
use App\Colaborador;
use App\Intervalo;
use App\setor;
use DB;
use Auth;
use Illuminate\Support\Collection;
use Illuminate\Support\Arr;
use Carbon\Carbon;
use DateTime;
use App\RelatorioPausa;
use app\views;
use View;
//use Dompdf\Dompdf;
//use Barryvdh\DomPDF\Facade as PDF;
use Knp\Snappy\Pdf ;


class RelatoriosController extends Controller
{
    public function pausacreate(){
      $colaboradores = Colaborador::all();
    	return view('relatorio.pausacreate',['colaboradores' => $colaboradores]);
    }

    public function anormalidadecreate(){
      $colaboradores = Colaborador::all();

      

    	return view('relatorio.anormalidadecreate',['colaboradores' => $colaboradores]);
    }

    public function pausa(Request $request){

        // $colaborador_id=$request->colaborador_id;
        // $colaborador = Colaborador::findOrFail($colaborador_id);
        
        if($colaborador_id=$request->colaborador_id==0){

          $colab = new Colaborador();
          $colaboradores = $colab::where('tag','<>',null)->get();
          //todos colaboradores que tiverem tag, ou seja terão o dispositívo de RFID terão relatório de pausa dos acessos aos seus respectivos setores.
          $html = '';
          
          foreach($colaboradores as $colaborador){
            $colaborador_id = $colaborador->matricula;
            $datainicio = $request->datainicio;
            $dataini = new DateTime($datainicio);
            $dataini = date_format($dataini, 'd/m/Y H:i:s');
            $datafinal = $request->datafinal;
            $datafinal = new DateTime($datafinal);
            $datafinal = date_time_set($datafinal, 23, 59, 59);
            $listaintervalo = [];
            $listarelatorio = [];
            $arrayperiodos = array();
            $passagens = DB::table('passagems')->where('time_passagem','>',$datainicio)->where('time_passagem','<',$datafinal)->where('colaborador_id',$colaborador_id)->orderBy('time_passagem')->get();
            $porta = 0;
            $setor = $colaborador->setor;
            // $setor = setor::find($setor_id);
            $turno = $colaborador->turno;

            foreach($passagens as $p){
              if($p->porta == 'EX'){
                  if($porta == 1){
                    // porta=1 colaborador esta dentro
                    // porta=0 colaborador esta fora
                    // $intervalo->push([
                    //     ['data' => 'Linda', 'porta' => 14,'porta' => 14]
                    // ]);
                    // status= 1 colaborador fora do matadouro
                    // status= 0 colaborador dentro do matadouro
                    $intervalo = new Intervalo();
                    $intervalo->porta = $p->porta;
                    $intervalo->status = 1;
                    $intervalo->data = $p->time_passagem;
                    $listaintervalo[] = $intervalo;
                  }
                  $porta = 0;
              }elseif($p->porta =='IN'){
                  if($porta == 0){
                    // $intervalo->push([
                    //     ['name' => 'Diego', 'age' => 23]
                    // ]);
                    $intervalo = new Intervalo();
                    $intervalo->porta = $p->porta;
                    $intervalo->status = 0;
                    $intervalo->data = $p->time_passagem;
                    $listaintervalo[] = $intervalo;
                  }
                  $porta=1;
                }
            }

            $datainicio = new Carbon($datainicio);
            $dataarray = [];
            $array = '';
            // $datafinal= new Carbon($request->datafinal);
            // $teste = array();
            // array_push($arr, 10);

            for($datainicio;$datainicio<=$datafinal;$datainicio->addDay()){
                $array = new Carbon($datainicio);
                $dataarray[] = $array;
            }

            foreach ($dataarray as $dia){
              $foi=0;
              $tempoinicial=0;
              $total=0;
              $inicio=0;
              $periodos= array();
              $arrayperiodos= array();

                foreach ($listaintervalo as $i){
                  $dateintervalo = new DateTime($i->data);
                  $dateintervalo = date_format($dateintervalo, 'd/m/y');
                  $diaarray = new DateTime($dia);
                  $diaarray = date_format($diaarray, 'd/m/y');

                  if($dateintervalo==$diaarray){
                    if($foi==1){
                        $periodos[]=$i->data;
                        if($i->status==1){
                            $tempoinicial=$i->data; 
                        }
                        if($i->status==0){
                            $tempofinal=$i->data;
                        }
                        else {
                          $tempofinal=$tempoinicial;
                        }
                        //precisa arrumar a diferença de tempo
                        $timefim = Carbon::createMidnightDate($tempofinal);
                        $timeinicio = Carbon::createMidnightDate($tempoinicial);
                        $diferenca=$timefim->diffInMinutes($timeinicio); 
                        $total=$total+$diferenca;         
                    } 
                              
                    if(($foi==0)&&($i->status==0)){
                      $inicio=$i->data;
                      $foi=1;
                    }
                  }
                }
                
                if($total>0){
              
                foreach ($periodos as $peri){
                  $pformat = new DateTime($peri);
                  $pformat=date_format($pformat, 'H:i:s');
                  $arrayperiodos[]=$pformat;
                }

                $relatorio = new RelatorioPausa();
                $dia = new DateTime($dia);
                $dia = date_format($dia, 'd/m/yy');
                $relatorio->dia = $dia;
                $inicioformat = new DateTime($inicio);
                $inicioformat = date_format($inicioformat, 'H:i:s');
                $relatorio->inicio = $inicioformat;
                $total=$total*60;
                $total= gmdate("H:i:s", $total);
                $relatorio->total = $total;
                $relatorio->periodos = $arrayperiodos;
                $listarelatorio[] = $relatorio;
              } 
            }

//$datafinal = new DateTime($datafinal);

            $datafinal = date_format($datafinal, 'd/m/Y H:i:s');

// dd($listarelatorio);
// return PDF::setOptions(['isRemoteEnabled' => true])->loadView('relatorio.pausa1',['colaborador' => $colaborador,'dataini' => $dataini,'datafinal' => $datafinal,'setor' => $setor,'turno' => $turno,'listarelatorio' => $listarelatorio])
//                ->stream();
// return view('relatorio.pausa',['colaborador' => $colaborador,'datainicio' => $datainicio,'datafinal' => $datafinal,'setor' => $setor,'turno' => $turno,'listarelatorio' => $listarelatorio]);
    
            $snappy = new Pdf('C:\wkhtmltopdf\bin\wkhtmltopdf');
            //$snappy = new Pdf('C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf');
            header('Content-Type: application/pdf');
            $view = view('relatorio.testepdf', ['colaborador' => $colaborador,'dataini' => $dataini,'datafinal' => $datafinal,'setor' => $setor,'turno' => $turno,'listarelatorio' => $listarelatorio]);
            $html .= $view;
          }
// $snappy->addPage('relatorio.pausa1', ['colaborador' => $colaborador,'dataini' => $dataini,'datafinal' => $datafinal,'setor' => $setor,'turno' => $turno,'listarelatorio' => $listarelatorio]);
// $html+= View::make('relatorio.pausa',['colaborador' => $colaborador,'datainicio' => $datainicio,'datafinal' => $datafinal,'setor' => $setor,'turno' => $turno,'listarelatorio' => $listarelatorio]);
// $path = base_path('public/uploads/files/');
// $pdf_name = time().'.pdf';
// $upload = \PDF::loadView('relatorio.pausa1', ['colaborador' => $colaborador,'dataini' => $dataini,'datafinal' => $datafinal,'setor' => $setor,'turno' => $turno,'listarelatorio' => $listarelatorio])->storeAs('products', $pdf_name);
//  \PDF::loadView('relatorio.pausa1', ['colaborador' => $colaborador,'dataini' => $dataini,'datafinal' => $datafinal,'setor' => $setor,'turno' => $turno,'listarelatorio' => $listarelatorio])->stream("$colaborador_id.pdf");
  
// $pdf = \PDF::loadView('relatorio.pausa1', ['colaborador' => $colaborador,'dataini' => $dataini,'datafinal' => $datafinal,'setor' => $setor,'turno' => $turno,'listarelatorio' => $listarelatorio]);
// download pdf
    
// $view = \View::make('relatorio.pausa1', ['colaborador' => $colaborador,'dataini' => $dataini,'datafinal' => $datafinal,'setor' => $setor,'turno' => $turno,'listarelatorio' => $listarelatorio]);
// $view->render();
// coloque nessa variável o código HTML que você quer que seja inserido no PDF
// carregamos o código HTML no nosso arquivo PDF

// $dompdf = new Dompdf();
// $dompdf->loadHtml($html);
// $dompdf->setPaper('letter');
// $dompdf->render();
// Page numbers
// $font = $dompdf->getFontMetrics()->getFont("Arial", "bold");
// $dompdf->getCanvas()->page_text(16, 770, "Page: {PAGE_NUM} of {PAGE_COUNT}", $font, 8, array(0, 0, 0));
// echo $dompdf->output();

            header('Content-Type: application/pdf');
            echo $snappy->getOutputFromHtml($html);

// Render the HTML as PDF
// $dompdf->render();

// Output the generated PDF to Browser

// PDF::loadHTML($html)->stream('download.pdf');
// PDF::loadHTML($html)->setPaper('a4', 'landscape')->save('myfile.pdf');

        }

        else{
          $colaborador_id = $request->colaborador_id;
          $colaborador = Colaborador::findOrFail($colaborador_id);
          $datainicio = $request->datainicio;
          $dataini = new DateTime($datainicio);
          $dataini = date_format($dataini, 'd/m/Y H:i:s');
          $datafinal = $request->datafinal;
          $datafinal = new DateTime($datafinal);
          $datafinal = date_time_set($datafinal, 23, 59,59);
          $listaintervalo = [];
          $listarelatorio = [];
          $arrayperiodos = array();
          $passagens = DB::table('passagems')->where('time_passagem','>',$datainicio)->where('time_passagem','<',$datafinal)->where('colaborador_id',$colaborador_id)->orderBy('time_passagem')->get();
          $porta = 0;
          $setor = $colaborador->setor;
          // $setor = setor::find($setor_id);
          $turno = $colaborador->turno;
  
          foreach($passagens as $p){
              if($p->porta == 'EX'){
                if($porta == 1){
                  // porta=1 colaborador esta dentro
                  // porta=0 colaborador esta fora
                  // $intervalo->push([
                  //     ['data' => 'Linda', 'porta' => 14,'porta' => 14]
                  // ]);
                  // status= 1 colaborador fora do matadouro
                  // status= 0 colaborador dentro do matadouro
                  $intervalo = new Intervalo();
                  $intervalo->porta = $p->porta;
                  $intervalo->status = 1;
                  $intervalo->data = $p->time_passagem;
                  $listaintervalo[] = $intervalo;
                }
                  $porta=0;
                }
                elseif($p->porta == 'IN'){
                  if($porta == 0){
                    // $intervalo->push([
                    //     ['name' => 'Diego', 'age' => 23]
                    // ]);
                    $intervalo = new Intervalo();
                    $intervalo->porta = $p->porta;
                    $intervalo->status = 0;
                    $intervalo->data = $p->time_passagem;
                    $listaintervalo[] = $intervalo;
                  }
                  $porta = 1;
                }
          }
          
          //teste para a contagem do numero de intervalos

          $datainicio = new Carbon($datainicio);
          // $datafinal= new Carbon($request->datafinal);
          $dataarray = [];
          $array = '';
          // $teste = array();
          // array_push($arr, 10);
          // dataarray cria um array adicionando as passagens de todos os dias da data inicial até a data final 
          for($datainicio;$datainicio<=$datafinal;$datainicio->addDay()){
              $array = new Carbon($datainicio);
              $dataarray[] = $array;
          }

          foreach ($dataarray as $dia){
            $foi=0;
            $tempoinicial=0;
            $total=0;
            $inicio=0;
            $periodos= array();
            $arrayperiodos= array();
  
            foreach ($listaintervalo as $i){

              $dateintervalo = new DateTime($i->data);
              $dateintervalo = date_format($dateintervalo, 'd/m/y');
              $diaarray = new DateTime($dia);
              $diaarray = date_format($diaarray, 'd/m/y');
            
              if($dateintervalo == $diaarray){
                if($foi == 1){
                  $periodos[]=$i->data;
                  if($i->status==1){
                    $tempoinicial=$i->data;
                  }
                  if($i->status==0){
                    $tempofinal=$i->data;
                  }
                  else{
                    $tempofinal=$tempoinicial;  
                  }
                  //precisa arrumar a diferença de tempo
                  $timefim = Carbon::createMidnightDate($tempofinal);
                  $timeinicio = Carbon::createMidnightDate($tempoinicial);
                  $diferenca=$timefim->diffInMinutes($timeinicio); 
                  $total=$total+$diferenca;
                  } 
                          
                if(($foi==0)&&($i->status==0)){
                  $inicio=$i->data;
                  $foi=1;
                }
              }
            }

            if($total>0){
            
              foreach ($periodos as $peri){            
                $pformat = new DateTime($peri);
                $pformat=date_format($pformat, 'H:i:s');
                $arrayperiodos[]=$pformat;
              }
              $relatorio = new RelatorioPausa();
              $dia = new DateTime($dia);
              $dia = date_format($dia, 'd/m/yy');
              $relatorio->dia = $dia;
              $inicioformat = new DateTime($inicio);
              $inicioformat = date_format($inicioformat, 'H:i:s');
              $relatorio->inicio = $inicioformat;
              $total = $total*60;
              $total = gmdate("H:i:s", $total);
              $relatorio->total = $total;
              $relatorio->periodos = $arrayperiodos;
              $listarelatorio[] = $relatorio;
            } 
          }

          //$datafinal = new DateTime($datafinal);
          $datafinal = date_format($datafinal, 'd/m/Y H:i:s');
  
          $snappy = new Pdf('C:\wkhtmltopdf\bin\wkhtmltopdf');
          //$snappy = new Pdf('C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf');

          $view = \View::make('relatorio.testepdf', ['colaborador' => $colaborador,'dataini' => $dataini,'datafinal' => $datafinal,'setor' => $setor,'turno' => $turno,'listarelatorio' => $listarelatorio])->render();
  
          header('Content-Type: application/pdf');
  
          echo $snappy->getOutputFromHtml($view);    
        }

    } 

    public function tags(){
     
      $colaboradores = Colaborador::where('tag', null)->orderBy('matricula', 'desc')->get();
      
      return view('relatorio.tags', ['colaboradores' => $colaboradores]);
    }
  
  }