<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Colaborador;
use App\Setor;
use App\Turno;


use DB;

class ColaboradorController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function index()
    {
    	$lista_colaboradores = Colaborador::all();

    	return view('colaborador.index', ['colaboradors' => $lista_colaboradores]);
    }



    public function create(){
        $setores = Setor::all();
        $turnos = Turno::all();

        return view('colaborador.create', ['setores' => $setores, 'turnos' => $turnos]);
    }

    public function store(Request $request){
        $colaborador = new colaborador();
        $colaborador->matricula = $request->matricula;
        $colaborador->nome = $request->nome;
        $colaborador->cod_setor = $request->cod_setor;
        $colaborador->ativo = $request->ativo;
        $colaborador->turno_id = $request->turno_id;
        $colaborador->cod_funcao = $request->cod_funcao;
        $colaborador->nome_funcao = $request->nome_funcao;
        $colaborador->data_admissao = $request->data_admissao;
        $colaborador->data_cadastro = $request->data_cadastro;
        $colaborador->cod_situacao = $request->cod_situacao;
        $colaborador->tag = $request->tag;

    	$colaborador->save();

    	return redirect('/colaborador')->with('message', 'colaborador registrado com sucesso!');
    
        
    }
    // END CREATE

    
    // UPDATE
    public function updateView($colaborador_id){
    	$colaborador = Colaborador::find($colaborador_id);
    	
    	return view('colaborador.update', ['colaborador' => $colaborador]);
    }

    public function update(Request $request){
        $colaborador = Colaborador::findOrFail($request->matricula);
        $colaborador->tag = $request->tag;
        $colaborador->save();
        return back()->with('success','TAG cadastrada com sucesso!');

    }
    // END UPDATE

  
    // DETAILED
    public function detailed($colaborador_id){
        $colaborador = colaborador::find($colaborador_id);
        return view('colaborador.detailed', ['colaborador' => $colaborador]);
    }
    // END DETAILED


    public function destroy($id)
    {
        $colaborador = colaborador::findOrFail($id);
        $colaborador->delete();
        return redirect()->route('colaborador.index')->with('alert-success','Colaborador deletado!');
    } 
}
