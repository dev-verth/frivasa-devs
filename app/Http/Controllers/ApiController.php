<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Colaborador;
use App\Setor;
use App\Turno;
use DB;
use App\Intervalo;
use App\Passagem;
use App\Events\PassagemFrivasa;
use Carbon\Carbon;
use DateTime;


class ApiController extends Controller
{
    public function verificaApi($tag)
    {
       $colaborador=DB::table('colaboradors')->where('tag',$tag)->get();
// dd($colaborador);
        if (empty($colaborador[0])) {
            return 0;
        }
        else
            return 1;
    }

    // public function ApiPassagem($id,$portal,$timep,$porta)
    // {

    //     $col = DB::table('colaboradors')->where('tag',$id)->get();

    //     $id=$col[0]->matricula;
    //     $situacao=$col[0]->cod_situacao;
    //     $turno=$col[0]->turno_id;

    //     if($id > 0)
    //     {
    //         //objeto passagem
    //         $passagem = new Passagem();
    //         $passagem->colaborador_id = $id;
    //         $passagem->portal_id = $portal;
    //         $passagem->cod_situacao = $situacao;
    //         $passagem->turno = $turno;
    //         $passagem->time_passagem = $timep;
    //         $passagem->porta = $porta;
    //         $passagem->save();

    //     }
       
    //     $id = strval($id);
    //     $portal = strval($portal);
    //     event(new PassagemFrivasa($id,$portal));  
    // } 

    public function ApiPassagem($id,$portal,$timep,$porta)
    {

        $today = Carbon::today('America/Sao_Paulo');

        $now = Carbon::now();

        //$horapassagem = Carbon::parse($timep)->format('H:i:s');

        $listaintervalos = DB::table('intervalos')->where('data',$today)->where('colaborador_id',$id)->get();

        $col = DB::table('colaboradors')->where('tag',$id)->get();

        $id=$col[0]->matricula;
        $situacao=$col[0]->cod_situacao;
        $turno=$col[0]->turno_id;

        if($id > 0)
        {
            //objeto passagem
            $passagem = new Passagem();
            $passagem->colaborador_id = $id;
            $passagem->portal_id = $portal;
            $passagem->cod_situacao = $situacao;
            $passagem->turno = $turno;
            $passagem->time_passagem = $timep;
            $passagem->porta = $porta;
            $passagem->save();

            $today = new DateTime($today);

            $timep = new DateTime($timep);

            $datetoday = date_format($today, 'd/m/y'); 

            $datepassagem = date_format($timep, 'd/m/y');

            $horapassagem = date_format($timep, 'H:i:s');

            if(($datepassagem == $datetoday) && ($listaintervalos->isEmpty()) && ($passagem->porta == "IN")){
                    //objeto intervalo
                    $intervalo = new Intervalo();
                    $intervalo->data = $today;
                    $intervalo->entrada = $horapassagem;
                    $intervalo->colaborador_id = $id;
                    //indicação do inicio de expediente
                    $intervalo->inicio = $now;
                    $intervalo->save();
            }else {
                //pegar ultima passagem do dia do colaborador:
                $ultimoelementocadastrado = end($listaintervalos);
                $ultimointervalo = $ultimoelementocadastrado[0];

                if($passagem->porta == 'EX'){
                    if($ultimointervalo->entrada == NULL){
                         
                      // porta=1 colaborador esta dentro
                      // porta=0 colaborador esta fora
                      // status= 1 colaborador fora do matadouro
                      // status= 0 colaborador dentro do matadouro
                      
                      //entrada pro intervalo
                      $ultimointervalo->entrada = $horapassagem;
                      $ultimointervalo->save();
                    }
                    else if($ultimointervalo->entrada != NULL){
                      $novointervalo = New Intervalo();  
                      $novointervalo->entrada = $horapassagem;
                      $novointervalo->save();
                    }
                   
                }elseif($passagem->porta == 'IN'){
                    if(($ultimointervalo->saida == NULL) && ($ultimointervalo->entrada != NULL)){
                        //saida do intervalo
                        $ultimointervalo->saida = $horapassagem;
                        $timefim = Carbon::createMidnightDate($ultimointervalo->saida);
                        $timeinicio = Carbon::createMidnightDate($ultimointervalo->entrada);
                        $duracao = $timefim->diffInMinutes($timeinicio);                     
                        $ultimointervalo->duracao = $duracao;
                        $ultimointervalo->save();
                    }
                }
                
            }
        }
    } 
}

