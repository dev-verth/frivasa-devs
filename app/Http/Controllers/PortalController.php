<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\portal;
use DB;

class PortalController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function index()
    {
    	$lista_portal = Portal::all();

    	return view('portal.index', ['portals' => $lista_portal]);
    }

   
  
// CREATE
    public function create(Request $request){
    	return view('portal.create');
    }

    public function store(Request $request){
        $portal = new portal();
        $portal->nome = $request->nome;
        $portal->save();

    	return redirect('/portal')->with('message', 'portal registrado com sucesso!');
    	
    }
    // END CREATE

    
    // UPDATE
    public function updateView($id_portal){
    	$portal = Portal::find($id_portal);
    	
    	return view('portal.update', ['portal' => $portal]);
    }

    public function update(Request $request){
        $portal = Portal::findOrFail($request->id);
        $portal->nome = $request->nome;
        $portal->save();

		return redirect("/portal")->with("message", "portal atualizado com sucesso!");
    }
    // END UPDATE

  
    // DETAILED
    public function detailed($id_portal){
        $portal = portal::find($id_portal);

        return view('portal.detailed', ['portal' => $portal]);
    }
    // END DETAILED

    public function destroy($id)
    {
        $portal = portal::findOrFail($id);
        $portal->delete();
        return redirect()->route('portal.index')->with('alert-success','portal deletado!');
    }
}
