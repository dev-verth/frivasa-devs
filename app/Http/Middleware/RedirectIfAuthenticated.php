<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use DateTime;


class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

      
         $now = new DateTime();
         $prazo = new DateTime('2020-10-25 00:00:00');

        if($now<=$prazo){
            if (Auth::guard($guard)->check()) {
                return redirect('/home');
            }
    
            return $next($request);
        }
        else{
            return redirect('/');
        }
        
    }
    
}
