<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelatorioPausa extends Model
{
    protected $fillable = [
    	'dia',
    	'inicio',
        'total',
        'periodos',
    ];
}
