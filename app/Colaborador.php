<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colaborador extends Model
{
    protected $primaryKey = 'matricula';
    public $timestamps = false;
    protected $fillable = [
    	'matricula',
    	'nome',
        'cod_setor',
        'ativo',
        'turno_id',
        'cod_funcao',
        'nome_funcao',
        'data_admissao',
        'data_cadastro',
        'cod_situacao',
    	'tag',
    ];

    protected $table = 'colaboradors';

    public function setor()
    {
        return $this->belongsTo(Setor::class,'cod_setor');
    }
    public function turno()
    {
        return $this->belongsTo(Turno::class,'turno_id');
    }
}
