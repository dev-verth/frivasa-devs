<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Intervalo extends Model
{
    //   protected $fillable = [
    // 	'porta',
    // 	'status',
    //  'data',
    // ];
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [     
        'id',
        'porta',
     	'status',
    	'data',
        'saida',
        'entrada',
        'inicio',
        'duracao',
        'colaborador_id',
    ];
    
    protected $table = 'intervalos';

    public function colaborador()
    {
        return $this->belongsTo(Colaborador::class,'colaborador_id');
    }
}
