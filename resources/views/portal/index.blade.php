@extends('layouts.master') 
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Portais</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Portais</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        
        <div class="m-2">
          <a href="/portal/create"><button  type="button" class="btn btn-block btn-primary">Novo</button></a>

        </div>
       
      
           





  
      </div>
 

     
            
           
          
          <div class="card direct-chat direct-chat-primary">
           
           
            <div class="card-body">
           
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nome</th>
                  <th></th>
                  <th></th>

                </tr>
                </thead>
                <tbody>
                  @foreach ($portals as $portal)
                <tr>
                  <td>{{ $portal->id }}</td>
                  <td>{{ $portal->nome }}</td>
                  {{-- <td><a href="/portal/{{$portal->id}}/destroy"><i class="fas fa-trash-alt"></i></a></td> --}}
                  <td><button onclick="confirmrota('/portal/{{$portal->id}}/destroy')"><i class="fas fa-trash-alt"></i></button></td>

                  <td><a href="/portal/{{$portal->id}}/update"><i class="fas fa-edit"></i></a></td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Nome</th>
                  <th></th>
                  <th></th>
                </tr>
                </tfoot>
              </table> 
            </div>
           
          </div>
         

          
        </section>
      
        <section class="col-lg-5 connectedSortable">

          

         
       
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
 
@section('javascript')

@extends('layouts.scripts')

@stop