@extends('layouts.master') 
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Novo Colaborador</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item ">Colaboradores</li>
            <li class="breadcrumb-item active">Novo</li>

          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  
  <section class="content">
    <div class="container-fluid">
     
      
        
        <!-- Input addon -->
        <div class="card card-info">
          <div class="card-header">
            <h5 class="card-title">Insira os dados do novo colaborador...</h5>
          </div>
          <form method="post" action="{{route('colaborador.store')}}">
            {!! csrf_field() !!}

          

            <div class="card-body">

            <label for="matricula">Matrícula : </label>
            <div class="input-group mb-3">
            <input type="number" name="matricula" class="form-control" placeholder="Digite o número de matricula do colaborador..." >  
            <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-check"></i></span>
            </div>
            </div>

            <label for="nome">Nome : </label>
            <div class="input-group mb-3">
            <input type="text" name="nome" class="form-control" placeholder="Digite o nome do colaborador...">  
            <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-signature"></i></span>
            </div>
            </div>

            <label for="cod_setor">Setor : </label>
            <div class="input-group mb-3">

              <select class="js-example-basic-single custom-select" name="cod_setor" >
                @foreach ($setores as $setor)
              <option value="{{$setor->codigo}}">{{$setor->nome}}</option>
                @endforeach
              </select>
            {{-- <input type="number" name="cod_setor" class="form-control" placeholder="Selecione o código do setor do colaborador...">   --}}
            <div class="input-group-append">
            <!-- <span class="input-group-text"><i class="fas fa-code"></i></span> -->
            </div>
            </div>

            <!-- <label for="ativo">Ativo : </label> -->
            <div class="input-group mb-3" style="display: none;">
            <input type="number" value="1" name="ativo" min="0" max="1" class="form-control" readonly>  
            <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-check"></i></span>
            </div>
            </div>

            <!-- <label for="turno_id">Turno : </label> -->
            <div class="input-group mb-3" style="display: none;">
              <select name="turno_id" class="js-example-basic-single custom-select mb-2">
                @foreach ($turnos as $turno)
              <option value="{{$turno->id}}" selected>{{$turno->descricao}}</option>
                @endforeach
              </select>      
              <div class="input-group-append">
            <!-- <span class="input-group-text"><i class="fas fa-user-clock"></i></span> -->
            </div>
            </div>

            <label for="cod_funcao">Código Função : </label>
            <div class="input-group mb-3">
            <input type="number" name="cod_funcao" class="form-control" placeholder="Digite o código da função do colaborador...">  
            <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-code"></i></span>
            </div>
            </div>

            <label for="nome_funcao">Função : </label>
            <div class="input-group mb-3">
            <input type="text" name="nome_funcao" class="form-control" placeholder="Digite o nome da função do colaborador...">  
            <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-award"></i></span>
            </div>
            </div>

            <label for="data_admissao">Data de admissão : </label>
            <div class="input-group mb-3">
            <input type="datetime-local"  name="data_admissao" class="form-control" value="2020-03-16T00:00" >  
            <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
            </div>
            </div>

            <label for="data_cadastro">Data de cadastro : </label>
            <div class="input-group mb-3">
            <input type="datetime-local"  name="data_cadastro" class="form-control" value="2020-03-16T00:00" >  
            <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
            </div>
            </div>

            {{-- <label for="cod_situacao">Código situação : </label> --}}
            <div class="input-group mb-3" style="display:none;">
            <input type="number" value="0" name="cod_situacao" class="form-control" placeholder="Digite o código da situação do colaborador...">  
            <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-code"></i></span>
            </div>
            </div>

            <label for="tag">TAG : </label>
            <div class="input-group mb-3">
            <input type="text" name="tag" class="form-control" placeholder="Digite a TAG do colaborador...">  
            <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-user-tag"></i></span>
            </div>
            </div>

            


            <div class="input-group mb-4">
              <button  type="submit" class="btn btn-md btn-success">Salvar</button>
            </div>
          </div>
            
        
          </form>
    
 
     
     
            
           
          
          <div class="card direct-chat direct-chat-primary">
           
           
            <div class="card-body">
           
             
            </div>
           
          </div>
         

          
        </section>
      
        <section class="col-lg-5 connectedSortable">

          

         
       
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
 
@section('javascript')
@extends('layouts.scripts')





@stop