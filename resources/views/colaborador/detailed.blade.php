@extends('layouts.master') 
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Colaborador: {{$colaborador->nome}}</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Colaborador</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  
  <section class="content">
    <div class="container-fluid">
     
      <div class="row">
        <div class="m-2">
          <a href=""><button  type="button" class="btn btn-block btn-primary">Relatório</button></a>

        </div>
        
      
      </div>
 
     
     
            
           
          
          <div class="card direct-chat direct-chat-primary">
           
           
            <div class="card-body">






           
 <!-- Widget: user widget style 1 -->
 <div class="card card-widget widget-user">
  <!-- Add the bg color to the header using any of the bg-* classes -->
  <div class="widget-user-header bg-info">
    <h3 class="widget-user-username">{{ $colaborador->nome }}<span style="margin-left: 30px">
      <td><button  class="btn btn-md btn-danger" onclick="confirmrota('/colaborador/{{$colaborador->matricula}}/destroy')"><i class="fas fa-trash-alt"></i></button></td>
      <td><a href="/colaborador/{{$colaborador->matricula}}/update"><button class="btn btn-md btn-success" ><i class="fas fa-edit"></i></button></a></td></span></h3>
    <h5 class="widget-user-desc">Matrícula: {{ $colaborador->matricula }}</h5>
    
  </div>
  {{-- <div class="widget-user-image">
    <img class="img-circle elevation-2" src="../dist/img/user1-128x128.jpg" alt="User Avatar">
  </div> --}}
  <div class="card-footer">
    <div class="row">
      <div class="col-sm-4 border-right">
        <div class="description-block">
          <h5 class="description-header">Setor: </h5>
          <span class="description-text">{{ $colaborador->cod_setor }}</span>
        </div>
        <!-- /.description-block -->
      </div>
      <!-- /.col -->
      <div class="col-sm-4 border-right">
        <div class="description-block">
          <h5 class="description-header">Ativo: </h5>
          <span class="description-text">{{ $colaborador->ativo }}</span>
        </div>
        <!-- /.description-block -->
      </div>
      <!-- /.col -->
      <div class="col-sm-4">
        <div class="description-block">
          <h5 class="description-header">Turno: </h5>
          <span class="description-text">{{ $colaborador->turno_id }}</span>
        </div>
        <!-- /.description-block -->
      </div>

        <!-- /.col -->
        <div class="col-sm-4 border-right">
          <div class="description-block">
            <h5 class="description-header">Função: </h5>
            <span class="description-text">{{ $colaborador->nome_funcao }}</span>
          </div>
          <!-- /.description-block -->
        </div>
          <!-- /.col -->
      <div class="col-sm-4 border-right">
        <div class="description-block">
          <h5 class="description-header">Data Admissão:</h5>
          <span class="description-text">{{ $colaborador->data_admissao }}</span>
        </div>
        <!-- /.description-block -->
      </div>

         <!-- /.col -->
         <div class="col-sm-4 border-right">
          <div class="description-block">
            <h5 class="description-header">Data Cadastro:</h5>
            <span class="description-text">{{ $colaborador->data_cadastro }}</span>
          </div>
          <!-- /.description-block -->
        </div>

           <!-- /.col -->
      <div class="col-sm-4 border-right">
        <div class="description-block">
          <h5 class="description-header">Situação:</h5>
          <span class="description-text">{{ $colaborador->cod_situacao }}</span>
        </div>
        <!-- /.description-block -->
      </div>

         <!-- /.col -->
         <div class="col-sm-4 border-right">
          <div class="description-block">
            <h5 class="description-header">TAG:</h5>
            <span class="description-text">{{ $colaborador->tag }}</span>
          </div>
          <!-- /.description-block -->
        </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
</div>
<!-- /.widget-user -->



            </div>
           
          </div>
         

          
        </section>
      
        <section class="col-lg-5 connectedSortable">

          

         
       
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
 
@section('javascript')
@extends('layouts.scripts')

@stop