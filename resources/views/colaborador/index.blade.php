@extends('layouts.master') 
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Colaboradores</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Colaboradores</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        
        <div class="m-2">
          <a href="/colaborador/create"><button  type="button" class="btn btn-block btn-primary">Novo</button></a>

        </div>
       
      
           





  
      </div>
 

     
            
           
          
          <div class="card direct-chat direct-chat-primary">
           
           
            <div class="card-body">
           
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Matricula</th>
                  <th>Nome</th>
                  <th>Setor</th>
                  {{-- <th>Ativo</th> --}}
                  <th>Turno</th>
                  {{-- <th>Situação</th> --}}
                  <th></th>
                  <th></th>

                </tr>
                </thead>
                <tbody>
                  @foreach ($colaboradors as $colab)
                <tr>
                  <td>{{ $colab->matricula }}</td>
                  <td><a href="/colaborador/{{$colab->matricula}}/detailed">{{ $colab->nome }}</a></td>
                  <td>{{ $colab->setor->nome }}</td>
                  {{-- <td>{{ $colab->ativo }}</td> --}}
                  <td>{{ $colab->turno->descricao }}</td>
                  {{-- <td>{{ $colab->cod_situacao }}</td> --}}
                  
                  {{-- <td><a href="/colaborador/{{$colab->matricula}}/destroy"  ><i class="fas fa-trash-alt"></i></a></td> --}}
                  <td><button onclick="confirmrota('/colaborador/{{$colab->matricula}}/destroy')"><i class="fas fa-trash-alt"></i></button></td>
                  <td><a href="/colaborador/{{$colab->matricula}}/update"><i class="fas fa-edit"></i></a></td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>Matricula</th>
                  <th>Nome</th>
                  <th>Setor</th>
                  {{-- <th>Ativo</th> --}}
                  <th>Turno</th>
                  {{-- <th>Situação</th> --}}
                  <th></th>
                  <th></th>
                </tr>
                </tfoot>
              </table> 
            </div>
           
          </div>
         

          
        </section>
      
        <section class="col-lg-5 connectedSortable">

          

         
       
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
 
@section('javascript')

@extends('layouts.scripts')

@stop