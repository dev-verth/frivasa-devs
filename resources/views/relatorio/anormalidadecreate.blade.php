@extends('layouts.master') 
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Relatório de Anormalidade</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/home">Home</a></li>
            <li class="breadcrumb-item active">Relatórios</li>
            <li class="breadcrumb-item active">Anormalidade</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  
  <section class="content">
    <div class="container-fluid">

        <div class="card card-warning">
          <div class="card-header">
            <h3 class="card-title">Insira as informações para gerar o relatório...</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form role="form" method="post" action="{{route('relatorio.anormalidade')}}" target="_blank">
              {!! csrf_field() !!}
              <div class="row">
                <div class="col">
                 
              <!-- input states -->
              <div class="form-group">
                <label class="col-form-label" for="inputSuccess"><i class="fas fa-check"></i> Data Inicio...</label>
                <input name="datainicio" id="today" type="date" class="form-control is-valid" id="inputSuccess" value="timestamp">
                <label class="col-form-label" for="inputSuccess"><i class="fas fa-check"></i> Data Final...</label>
                <input name="datafinal" id="todays" type="date" class="form-control is-valid" id="inputSuccess" value="timestamp">
              </div>
             
            </div>
          </div>
            
          <div class="row">
          
            <div class="col">
              <!-- select -->
              <!-- <div class="form-group">
                <label>Selecione o colaborador com algum tipo de comportamento inesperado</label>
                <select name="colaborador_id" class="js-example-basic-single custom-select">
                  <option value="0" selected>Todos Colaboradores</option>
                    @foreach ($colaboradores as $colaborador)
                      <option value="{{$colaborador->matricula}}">{{$colaborador->nome}} - {{$colaborador->matricula}}</option>
                    @endforeach
                </select>
              </div> -->
            </div>
            
              </div>
              <div class="input-group mb-3">
                <button  type="submit" target="_blank" class="btn btn-md btn-success">Gerar Relatório</button>
              </div>
            </div>
           
            </form>
          </div>
          <!-- /.card-body -->
        </div>

      </div>
          
          <div class="card direct-chat direct-chat-primary">
           
           
            <div class="card-body">
           
            </div>
           
          </div>

          
        </section>
      
        <section class="col-lg-5 connectedSortable">

        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
 
@section('javascript')

@extends('layouts.scripts')
<script>
let today = new Date().toISOString().substr(0, 10);
document.querySelector("#today").value = today;
let todays = new Date().toISOString().substr(0, 10);
document.querySelector("#todays").value = todays;
</script>

@stop