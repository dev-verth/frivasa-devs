
@extends('layouts.master') 
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Relatório de Pausa</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Relatório de pausa</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        
        {{-- <div class="m-2">
          <a href="/colaborador/create"><button  type="button" class="btn btn-block btn-primary">Novo</button></a>

        </div> --}}
  
      </div>
 
          <div class="card direct-chat direct-chat-primary">
              <div class="col-sm-6">
                <p>Frigorifico do vale do sapucai LTDA </p>
              </div>

              <div class="col-sm-6">
                <p style="text-align: right">4564565</p>
              </div>

              <div class="col-sm-12">
                <p>Itajubá - MG <span style="text-align: right"></span></p>
                <h4 style="text-align: center">Comprovante de Pausas</h4>
                <p>Funcionário:  <span style="text-align: right">{{ $colaborador->nome }} </span></p>
                <p>Setor:  <span style="text-align: right">{{ $setor->codigo }}</span></p>
                <p>Turno:  <span style="text-align: right"> {{ $turno->id }}</span></p>
                <p>Período:  <span style="text-align: right"> {{ $datainicio }} - {{ $datafinal }}</span></p>
              </div>
           
            <div class="card-body">
           
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Dia</th>
                  <th>Inicio</th>
                  <th>Total</th>
                  <th>Períodos</th>
                </tr>
                </thead>
              <tbody>

              @foreach ($listarelatorio as $lista)
                <tr>
                  <td>{{$lista->dia}}</td>
                  <td>{{$lista->inicio}}</td>
                  <td>{{$lista->total}}</td>
                  <td>
                    @foreach ($lista->periodos as $p)
                      {{$p}} - 
                    @endforeach
                  </td>
                </tr>
              @endforeach
                </tbody>

                <tfoot>
                <tr>
                  <th>Dia</th>
                  <th>Inicio</th>
                  <th>Total</th>
                  <th>Períodos</th>
                </tr>
                </tfoot>
              </table> 
            </div>
          </div>

        </section>
      
        <section class="col-lg-5 connectedSortable">

        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
 
@section('javascript')

@extends('layouts.scripts')

@stop