
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        {{-- <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"> --}}
        {{-- <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> --}}
        {{-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"> --}}
    
        {{-- <link href="{{ asset('css/relatorioboot.css') }}" rel="stylesheet"> --}}
        {{-- <link href="{{ asset('css/familylato.css') }}" rel="stylesheet">
        <link href="{{ asset('css/familysans.css') }}" rel="stylesheet"> --}}
    
    
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
    
        <title>Relatorio de pausa</title>
        
        {{-- <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/familylato.css" rel="stylesheet">
        <link href="css/familysans.min.css" rel="stylesheet"> --}}
    </head>
    <style>

    
  /* @page {
	header: page-header;
	footer: page-footer;
} */
      
      /* #footer { 
        page-break-before: always;
        position: fixed;
bottom: 0;
width: 100%;
      } */
     
      .page_break { page-break-before: always; }
      
      </style>

    <body>
 
    {{-- <htmlpageheader name="page-header"> --}}
    <hr style="border-top: dashed 1px; width: 900px; color: gray; clear: both;">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-12">
              <h2 class="m-0 text-dark" style="text-align: center;">Relatório de Pausas</h2>
            </div>
            <!-- /.col -->
           
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
      </div>
      
      <section class="content">
        <div class="container-fluid">
    
          <div class="row">
     
          </div>
     
              <div class="card direct-chat direct-chat-primary">
               
                  <div style="float: left;">
                    <p>Frigorifico do vale do sapucai LTDA </p>
                    </div>
                    <div style="float: right;">
                      <p >CNPJ : 01.702.122/0001-92</p>
                    </div>
                  </div>
    
                  
    <br>
    <br>
    
    <div class="card direct-chat direct-chat-primary">
                    <?php
                    date_default_timezone_set('America/Sao_Paulo');
                    $dataatual=date('d/m/Y \à\s H:i:s');
                    ?>
    
                     <div style="float: left;">
                      <p>Itajubá - MG </p>
                      </div>
                      <div style="float: right;">
                        <p >{{$dataatual}}</p>
                      </div>
                  
                  </div>
    
    
                                 
                  <div class="col-sm-12">
                   
    
                    <h4 style="text-align: center">Comprovante de Pausas</h4>
                    <p>Funcionário:  <span style="text-align: right">{{ $colaborador->matricula }} - {{ $colaborador->nome }} </span></p>
                    <p>Setor:  <span style="text-align: right">{{ $setor->codigo }} - {{ $setor->nome }}</span></p>
                    <p>Turno:  <span style="text-align: right"> {{ $turno->id }} - {{ $turno->descricao }}</span></p>
                    <p>Período:  <span style="text-align: right"> {{ $dataini }} Até {{ $datafinal }}</span></p>
    
                    </div>
                <div class="card-body">
               
                  <table  style=" border: 1px solid #ccc;
                  width: 100%;
                  margin:0;
                  padding:0;
                  border-collapse: collapse;
                  border-spacing: 0;">
                    <thead>
                    <tr style=" border: 1px solid #ddd;
                    padding: 5px;">
                      <th style="text-transform: uppercase;
                      font-size: 14px; padding: 10px;
    text-align: center;
                      letter-spacing: 1px;">Dia</th>
                      <th style="text-transform: uppercase;
                      font-size: 14px; padding: 10px;
    text-align: center;
                      letter-spacing: 1px;">Inicio</th>
                      <th style="text-transform: uppercase;
                      font-size: 14px; padding: 10px;
    text-align: center;
                      letter-spacing: 1px;">Total</th>
                      <th style="text-transform: uppercase;
                      font-size: 14px; padding: 10px;
    text-align: center;
                      letter-spacing: 1px;">Períodos</th>
                    </tr>
                    </thead>
                    <tbody>
    <?php
    $count=0;
    ?>
      @foreach ($listarelatorio as $lista)
              <tr style=" border:1px solid #ddd;
              padding: 5px;">
              <td style="padding: 10px;
              text-align: center;" WIDTH=40>{{$lista->dia}}</td>
                <td style="padding: 10px;
                text-align: center;" WIDTH=90>{{$lista->inicio}}</td>
                <td style="padding: 10px;
                text-align: center;" WIDTH=30>{{$lista->total}}</td>
                <td style="padding: 10px;
                text-align: center;" WIDTH=300 >
                  @foreach ($lista->periodos as $p)
                  <?php
                  $count=$count+1;
                  ?>
                  {{$p}} 
    
                  @if($count==1)
                  @if ($loop->last)
                   Saiu
                  @else
                  Até
                  @endif
                  @endif
                  @if($count==2)
                  ; 
                  <?php
                  $count=0;
                  ?>
                  @endif
                  @endforeach
                   
                </td>
              </tr>
            
     @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                      <th>Dia</th>
                      <th>Inicio</th>
                      <th>Total</th>
                      <th>Períodos</th>
                  
                    </tr>
                    </tfoot>
                  </table> 
                </div>
               
              </div>
              {{-- </htmlpageheader> --}}


              {{-- <htmlpagefooter name="page-footer"> --}}
              <footer style="position: relative; margin-top: 650px">
                <p>
                  Nos termos da NR n° 36-Seg.e Saúde no trabalho em empresa de abate e Processamento de Carnes e Derivados , aprovada pelo MTE, estou de pleno acordo com o que demonstram as marcações de pausas citadas acima, sendo que representam o ocorrido neste período.
                </p>
              <table style="width:100%">
                {{-- <tr>
                <td><strong>
                Assinatura do empregado: </strong>_________________________________
                </td>
                </tr> --}}
                <tr>
                  <td style="text-align: center"> ___________________________________________ 
                   
                  </td>
                  <td style="text-align: left">Data: ____/____/____</td>
                </tr>
                <tr>
                  <td style="text-align: center">
                   {{$colaborador->nome}}
                  </td>
                  <td ></td>
                </tr>
              
                <tr>
                  <td >
          
                  </td>
                  <td ></td>
                </tr>
    
    
                <tr>
                  <td style="text-align: center"> ___________________________________________ 
                   
                  </td>
                  <td style="text-align: left">Data: ____/____/____</td>
                  </tr>
                  <tr>
                    <td style="text-align: center">
                      Assinatura do Gestor
                    </td>
                    <td ></td>
                  </tr>
                </table>
              </footer>
              {{-- </htmlpagefooter> --}}

            </section>
       
            <!-- right col -->
          </div>
          <!-- /.row (main row) -->
        </div>
        <!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    
    </body>
    <div class="page_break"></div>
    </html>
    