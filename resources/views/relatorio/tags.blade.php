@extends('layouts.master') 
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          @include('flash-message')


@yield('content')
          <h1 class="m-0 text-dark">Colaboradores sem TAG cadastrada</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Colaboradores</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        
        {{-- <div class="m-2">
          <a href="/colaborador/create"><button  type="button" class="btn btn-block btn-primary">Novo</button></a>

        </div> --}}
       
      
           





  
      </div>
 

     
            
           
          
          <div class="card direct-chat direct-chat-primary">
           
           
            <div class="card-body">
           
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Matricula</th>
                  <th>Nome</th>
                  <th>Setor</th>
                  <th>Ativo</th>
                  <th>Turno</th>
                  <th>Situação</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($colaboradores as $colab)
                <tr>
                  <td>{{ $colab->matricula }}</td>
                  <td><a href="/colaborador/{{$colab->matricula}}/detailed">{{ $colab->nome }}</a></td>
                  <td>{{ $colab->cod_setor }}</td>
                  <td>{{ $colab->ativo }}</td>
                  <td>{{ $colab->turno_id }}</td>
                  <td>{{ $colab->cod_situacao }}</td>
                  
                  {{-- <td><a href="/colaborador/{{$colab->matricula}}/destroy"  ><i class="fas fa-trash-alt"></i></a></td> --}}
                  <td><button type="button" class="btn btn-info" data-toggle="modal" onclick="setaDadosModal('{{ $colab->matricula }}','{{ $colab->nome }}')" data-target="#modal-info"><i class="fas fa-tag"></i>
                    Inserir TAG
                  </button></td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>Matricula</th>
                  <th>Nome</th>
                  <th>Setor</th>
                  <th>Ativo</th>
                  <th>Turno</th>
                  <th>Situação</th>
                  <th></th>
                </tr>
                </tfoot>
              </table> 
            </div>          
          </div>         
        </section>
      
        <section class="col-lg-5 connectedSortable">

          
 <!-- /.modal -->

 <div class="modal fade" id="modal-info">
    <div class="modal-dialog">
      <div class="modal-content bg-default">
        <div class="modal-header">
          <h4 class="modal-title">Cadastrar TAG <i class="fas fa-tag"></i></h4>
          
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          
        </div>
        <div class="modal-body">
          <div id="id-info" class="alert alert-info alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong>Atenção Campo TAG vem preenchido para facilitar cadastro...</strong>
          </div>
            <form method="post" action="{{route('colaborador.update')}}">
                {!! csrf_field() !!}
      
              
      
                <div class="card-body">
      
                <label for="matricula">Matrícula: </label>
                <div class="input-group mb-3">
                <input type="number" name="matricula" style="display: block" id="campo" value="" class="form-control" placeholder="Digite o número de matricula do colaborador..." Readonly >  
                <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-check"></i></span>
                </div>
                </div>
      
                <label for="nome">Nome : </label>
          <div class="input-group mb-3">
          <input type="text" name="nome" value="" id="nome" class="form-control" placeholder="Digite o nome do colaborador..." Readonly>  
          <div class="input-group-append">
          <span class="input-group-text"><i class="fas fa-signature"></i></span>
          </div>
          </div>
               
      
                <label for="tag">TAG : </label>
                <div class="input-group mb-3">
                <input type="text" name="tag"  id="tag" class="form-control" placeholder="Digite o número de matricula do colaborador...">  
                <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-user-tag"></i></span>
                </div>
                </div>
      
                
      
      
                {{-- <div class="input-group mb-3">
                  <button  type="submit" class="btn btn-md btn-success">Salvar</button>
                </div> --}}
              </div>
                
            
              {{-- </form> --}}

        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-outline-success">Cadastrar</button>
        </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
         
       
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
 
@section('javascript')

@extends('layouts.scripts')
<script>
    function setaDadosModal(valor,valor2) {
        document.getElementById('campo').value = valor;
        document.getElementById('tag').value = valor;
        document.getElementById('nome').value = valor2;
    }

    setTimeout(() => {
  $('#id-success').alert('close');
}, 3000);
    </script>
@stop