@extends('layouts.master') 
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Passagens</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Passagens</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <br>
  <br>
  <section class="content">
    <div style="margin-top: 20px;" class="container-fluid">

      <div class="row">
        <div class="grid grid-mapa mb-5">
          <div class="row1" id="box1"></div>
          <div class="row1" id="box2"></div>
          <div class="row1" id="box3"></div>
          <div class="row2" id="box4"></div>
          <div class="row2" id="box5"></div>
        </div>
        <div class="m-2">
          {{-- <a href="/passagem/create"><button  type="button" class="btn btn-block btn-primary">Novo</button></a> --}}

        </div>
     
      </div>
 
          <div class="card direct-chat direct-chat-primary">
           
            <div class="card-body">

       
            </div>
           
          </div>
         
        </section>
      
        <section class="col-lg-5 connectedSortable">

          

         
       
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection 
@section('javascript')
@extends('layouts.scripts')
{{-- @extends('layouts.scripts') --}}
<script src="https://js.pusher.com/5.0/pusher.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <script type="text/javascript">
       function apagar60(idDiv) {
         document.getElementById(idDiv).remove();
       }
       Pusher.logToConsole = true;

      var pusher = new Pusher('7730baf8f1fa5c5df171',{
        cluster: 'mt1',
        encrypted: true
      });

  
      var channel = pusher.subscribe('passagemchannel');
      channel.bind('PassagemFrivasa', function(data){
        console.log("chegou aqui");
        var colaborador_id = data.col_id;
        var portal = data.portal;
       
        if(document.getElementById('icon'+colaborador_id) != null)
        {
          document.getElementById('icon'+colaborador_id).remove();
        }
        if (portal>0){
          var icon = document.createElement("i"); icon.id = "icon"+colaborador_id; icon.className = "fas fa-user-check fa-2x pointer"; icon.innerHTML = " ";icon.style="color:red;padding: 15px 15px 15px 15px"; icon.title=" "+ colaborador_id + " ";
          document.getElementById('box'+portal).append(icon);
        }else{
          var icon = document.createElement("i"); icon.id = "icon"+colaborador_id; icon.className = "fas fa-user-check fa-2x"; icon.innerHTML = "cancel"; icon.title=" "+ colaborador_id + " ";
          var coluna = document.getElementById('box'+portal);
          coluna.append(icon);
          }
        setTimeout(
        function(){
          if(document.getElementById('icon'+colaborador_id) != null)
            document.getElementById("icon"+colaborador_id).remove();
        },39000);
    });
    </script>
@stop