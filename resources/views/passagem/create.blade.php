@extends('layouts.master') 
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Nova Passagem</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item ">Passagens</li>
            <li class="breadcrumb-item active">Novo</li>

          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  
  <section class="content">
    <div class="container-fluid">
     
      
        
        <!-- Input addon -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Insira os dados da nova passagem...</h3>
          </div>
          <form method="post" action="{{route('passagem.store')}}">
            {!! csrf_field() !!}

          

            <div class="card-body">
{{-- necessario receber a lista de colaboradores --}}
            <label for="matricula">Colaborador : </label>
            <div class="input-group mb-3">
            <select name="colaborador_id" class="custom-select">
              @foreach ($colaboradores as $colaborador)
            <option value="{{$colaborador->matricula}}">{{$colaborador->nome}}</option>
              @endforeach
            </select>
            <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-check"></i></span>
            </div>
            </div>
{{-- necessario receber a lista de portais --}}

            <label for="nome">Portal  : </label>
            <div class="input-group mb-3">
            <select name="portal_id" class="custom-select">
              @foreach ($portais as $portal)
            <option value="{{$portal->id}}">{{$portal->nome}}</option>
              @endforeach
            </select>
            <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-check"></i></span>
            </div>
            </div>

            <label for="cod_setor">Situação : </label>
            <div class="input-group mb-3">
            <input type="number" name="cod_situacao" class="form-control" placeholder="Digite o número de matricula do colaborador...">  
            <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-check"></i></span>
            </div>
            </div>

            <label for="ativo">Turno  : </label>
            <div class="input-group mb-3">
            <input type="number" name="turno" class="form-control" placeholder="Digite o número de matricula do colaborador...">  
            <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-user-clock"></i></span>
            </div>
            </div>

            <label for="turno_id">Data de passagem : </label>
            <div class="input-group mb-3">
            <input type="datetime" name="time_passagem" value="2020-01-20 00:00:00" class="form-control" placeholder="Digite o número de matricula do colaborador...">  
            <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
            </div>
            </div>

            <label for="cod_funcao">Porta : </label>
            <div class="input-group mb-3">
            <input type="text" name="porta" class="form-control" placeholder="Digite o número de matricula do colaborador...">  
            <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-door-open"></i></span>
            </div>
            </div>


            <div class="input-group mb-3">
              <button  type="submit" class="btn btn-md btn-success">Salvar</button>
            </div>
          </div>
            
        
          </form>
    
 
     
     
            
           
          
          <div class="card direct-chat direct-chat-primary">
           
           
            <div class="card-body">
           
             
            </div>
           
          </div>
         

          
        </section>
      
        <section class="col-lg-5 connectedSortable">

          

         
       
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
 
@section('javascript')
<!-- jQuery -->
<script src="/dist/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)

</script>
<!-- Bootstrap 4 -->
<script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="/dist/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="/dist/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="/dist/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/dist/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="/dist/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="/dist/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="/dist/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="/dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="/dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/dist/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/dist/js/demo.js"></script>
@stop