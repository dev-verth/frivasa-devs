<!DOCTYPE html> 
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>teste</title>
    <link rel="stylesheet" href="/css/app.css">
    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/new.js') }}" defer></script> --}}
    <script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    
    <!-- Icons -->
    <script src="https://kit.fontawesome.com/5faedbee80.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
    <!-- Braian 08/08/2019 -->
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('css/grid.css') }}" > --}}
    <!-- Icones -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      


</head>
<body>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Passagens</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Passagens</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  
  <section class="content">
    <div style="margin-top: 20px;" class="container-fluid">

      <div class="row">
        <div class="grid grid-mapa mb-5">
          <div class="row1" id="box1"></div>
          <div class="row1" id="box2"></div>
          <div class="row1" id="box3"></div>
          <div class="row2" id="box4"></div>
          <div class="row2" id="box5"></div>
        </div>
        <div class="m-2">
          <a href="/passagem/create"><button  type="button" class="btn btn-block btn-primary">Novo</button></a>

        </div>
       
      
           





  
      </div>
 

     
            
           
          
          <div class="card direct-chat direct-chat-primary">
           
           
            <div class="card-body">

              





           
                
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://js.pusher.com/4.4/pusher.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script type="text/javascript">
       Pusher.logToConsole = true;


        // menu mobile
       

  // ÁREA NOTIFICAÇÃO EM RTA NO NAVEGADOR 

  // Os scripts abaixo são todos necessários para rodar o serviço do Pusher.
  // Porém pode-se ser necessário atualizar para novos CDNs à medida que os serviços também atualizam.
  // Estão sendo utilizado CDNs minificados.

        // Enable pusher logging - don't include this in production
        //Pusher.logToConsole = true;

    // Esse é o principal script do Pusher.     
    // Pusher é um serviço de redes distribuídas. 
    // Para mais informações acesse a plataforma do Pusher https://pusher.com/ e sua documentação.

    // Para configurar um novo serviço, será preciso entrar no canal, criar uma nova conta e canal
    // E a partir delas usar suas informações.  

    // Cria-se uma variável com nome e tipo Pusher, que recebe de primeiro argumento a chave,
    // E em segundo argumento recebe as características do Pusher, que no caso aqui são o cluster
    // E se está encriptado em valor de booleano.

    // Todos esses valores é possível encontrar dentro do seu canal criado dentro da plataforma do Pusher.                  

        var pusher = new Pusher('7730baf8f1fa5c5df171', {
        cluster: 'mt1',
        encrypted: true
        });

    // Pusher possui suporte para Laravel, e dentro do framework PHP usamos o modelo de eventos para o serviço
    // Cria-se uma variável do nome do canal, aqui channel, que se inscreve ao canal que deixamos específico
    // No nosso evento Laravel, no caso aqui 'passagem'.    

        // Subscribe to the channel we specified in our Laravel Event
        var channel = pusher.subscribe('passagemchannel');

    // Em seguida criamos um método, em que conectamos o canal ao evento por inteiro.
    // Recebendo como primeiro argumento a informação para conectar,
    // E em segundo argumento recebe uma nova função.

    // Essa segunda função é a que recebe os dados vindo do evento 'passagem',
    // E que realiza todo o trabalho na view sempre que aquele evento é acionado vindo do Pusher.        

        // Bind a function to a Event (the full Laravel class)
        channel.bind('App\\Events\\PassagemFrivasa', function(data){
          console.log(data);


        });

            
    </script>    


</body>
</html>