@extends('layouts.master') 
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Editar Passagem</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Passagens</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  
  <section class="content">
    <div class="container-fluid">
     
       <!-- Input addon -->
       <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">Edite os dados da passagem {{$passagem->id}} ...</h3>
        </div>
        <form method="post" action="{{route('passagem.update')}}">
          {!! csrf_field() !!}
          <div class="card-body">
          <label for="matricula">Id: </label>
          <div class="input-group mb-3">
          <input type="number" name="id" style="display: block" value="{{$passagem->id}}" class="form-control" placeholder="Digite o número de matricula do colaborador..." readonly>  
          <div class="input-group-append">
          <span class="input-group-text"><i class="fas fa-code"></i></span>
          </div>
          </div>

          <label for="nome">Colaborador : </label>
          <div class="input-group mb-3">
          <input type="number" name="colaborador_id" value="{{$passagem->colaborador_id}}" class="form-control" placeholder="Digite o número de matricula do colaborador..." readonly>  
          <div class="input-group-append">
          <span class="input-group-text"><i class="fas fa-check"></i></span>
          </div>
          </div>

          <label for="cod_setor">Portal : </label>
          <div class="input-group mb-3">
          <input type="number" name="portal_id" value="{{$passagem->portal_id}}" class="form-control" placeholder="Digite o número de matricula do colaborador..." readonly>  
          <div class="input-group-append">
          <span class="input-group-text"><i class="fas fa-code"></i></span>
          </div>
          </div>

          <label for="ativo">Situação : </label>
          <div class="input-group mb-3">
          <input type="number" name="cod_situacao"  value="{{$passagem->cod_situacao}}" class="form-control" placeholder="Digite o número de matricula do colaborador..." >  
          <div class="input-group-append">
          <span class="input-group-text"><i class="fas fa-check"></i></span>
          </div>
          </div>

          <label for="turno_id">Turno  : </label>
          <div class="input-group mb-3">
          <input type="number" name="turno" value="{{$passagem->turno}}" class="form-control" placeholder="Digite o número de matricula do colaborador..." >  
          <div class="input-group-append">
          <span class="input-group-text"><i class="fas fa-user-clock"></i></span>
          </div>
          </div>

          <label for="cod_funcao">Data de passagem : </label>
          <div class="input-group mb-3">
          <input type="datetime" name="time_passagem" value="{{$passagem->time_passagem}}" class="form-control" placeholder="Digite o número de matricula do colaborador..." readonly >  
          <div class="input-group-append">
          <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
          </div>
          </div>

          <label for="nome_funcao">Porta : </label>
          <div class="input-group mb-3">
          <input type="text" name="porta" value="{{$passagem->porta}}" class="form-control" placeholder="Digite o número de matricula do colaborador..." >  
          <div class="input-group-append">
          <span class="input-group-text"><<i class="fas fa-door-open"></i></span>
          </div>
          </div>

          <div class="input-group mb-3">
            <button  type="submit" class="btn btn-md btn-success">Salvar</button>
          </div>
        </div>
          
      
        </form>
 
     
     
            
           
          
          <div class="card direct-chat direct-chat-primary">
           
           
            <div class="card-body">
           
             
            </div>
           
          </div>
         

          
        </section>
      
        <section class="col-lg-5 connectedSortable">

          

         
       
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
 
@section('javascript')
<!-- jQuery -->
<script src="/dist/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)

</script>
<!-- Bootstrap 4 -->
<script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="/dist/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="/dist/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="/dist/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/dist/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="/dist/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="/dist/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="/dist/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="/dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="/dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/dist/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/dist/js/demo.js"></script>
@stop