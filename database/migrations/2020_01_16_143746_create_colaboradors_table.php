<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColaboradorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colaboradors', function (Blueprint $table) {
            $table->integer('matricula');
            $table->primary('matricula');
            $table->string('nome');
            $table->string('cod_setor');
            $table->foreign('cod_setor')->references('codigo')->on('setors');
            $table->boolean('ativo');
            $table->integer('turno_id')->unsigned();
            $table->foreign('turno_id')->references('id')->on('turnos');
            $table->integer('cod_funcao');
            $table->string('nome_funcao');
            $table->dateTime('data_admissao');
            $table->dateTime('data_cadastro');
            $table->integer('cod_situacao');
            $table->string('tag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colaboradors');
    }
}
