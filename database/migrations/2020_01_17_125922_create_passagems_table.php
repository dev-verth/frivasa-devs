<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePassagemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passagems', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('colaborador_id');
            $table->foreign('colaborador_id')->references('matricula')->on('colaboradors');
            $table->integer('portal_id')->unsigned();
            $table->foreign('portal_id')->references('id')->on('portals');
            $table->integer('cod_situacao');
            $table->integer('turno');
            $table->time('time_passagem');
            $table->string('porta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('passagems');
    }
}
