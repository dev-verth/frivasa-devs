<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeign3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('setorportal', function (Blueprint $table) {
            $table->integer( 'portal_id')->unsigned()->after('setor_id'); 
            $table->foreign('portal_id')->references('id')->on('portals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('setorportal', function (Blueprint $table) {
            //
        });
    }
}
