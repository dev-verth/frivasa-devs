<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntervalosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('intervalos', function (Blueprint $table) {
            $table->increments('id');
            $table->primary('id');
            $table->dateTime('data');
            $table->dateTime('saida');
            $table->dateTime('entrada');
            $table->dateTime('inicio');
            $table->dateTime('duracao');
            $table->integer('colaborador_id')->unsigned();
            $table->foreign('colaborador_id')->references('matricula')->on('colaboradors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('intervalos');
    }
}
