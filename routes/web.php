<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});
$now = new DateTime();
$prazo = new DateTime('2020-10-25 00:00:00');
if($now<=$prazo){
Auth::routes();
Route::get('/home', 'DashboardController@versionone')->name('home');
Route::get('/dashboard/home', 'DashboardController@versionone')->name('home');
Route::get('/dashboard/v2', 'DashboardController@versiontwo')->name('v2');
Route::get('/dashboard/v3', 'DashboardController@versionthree')->name('v3');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::group(array('prefix' => 'colaborador'), function()
{
    Route::get('/', 'ColaboradorController@index')->name('colaborador.index'); 
    Route::get('/create', 'ColaboradorController@create')->name('colaborador.create');
    Route::post('/store', 'ColaboradorController@store')->name('colaborador.store');
    Route::get('/{matricula}/update', 'ColaboradorController@updateView')->name('colaborador.updateView');
    Route::post('/update', 'ColaboradorController@update')->name('colaborador.update');
    Route::get('/{matricula}/detailed', 'ColaboradorController@detailed')->name('colaborador.detailed');
    Route::get('/{id}/destroy', 'ColaboradorController@destroy')->name('colaborador.destroy'); 
    Route::get('/verificaApi/{tag}', 'apicontroller@verificaApi')->name('colaborador.verificaApi'); 

    
});

Route::group(array('prefix' => 'portal'), function()
{
    Route::get('/', 'PortalController@index')->name('portal.index'); 
    Route::get('/create', 'PortalController@create')->name('portal.create');
    Route::post('/store', 'PortalController@store')->name('portal.store');
    Route::get('/{id}/update', 'PortalController@updateView')->name('portal.updateView');
    Route::post('/update', 'PortalController@update')->name('portal.update');
    Route::get('/{id}/detalhe', 'PortalController@detailed'); 
    Route::get('/{id}/destroy', 'PortalController@destroy')->name('portal.destroy');   
});

Route::group(array('prefix' => 'setor'), function()
{
    Route::get('/', 'SetorController@index')->name('setor.index'); 
    Route::get('/create', 'SetorController@create')->name('setor.create');
    Route::post('/store', 'SetorController@store')->name('setor.store');
    Route::get('/{codigo}/update', 'SetorController@updateView')->name('setor.updateView');
    Route::post('/update', 'SetorController@update')->name('setor.update');
    Route::get('/{codigo}/detalhe', 'SetorController@detailed'); 
    Route::get('/{codigo}/destroy', 'SetorController@destroy')->name('setor.destroy');    
});

Route::group(array('prefix' => 'passagem'), function()
{
    Route::get('/', 'PassagemController@index')->name('passagem.index'); 
    Route::get('/create', 'PassagemController@create')->name('passagem.create');
    Route::post('/store', 'PassagemController@store')->name('passagem.store');
    Route::get('/{id}/update', 'PassagemController@updateView')->name('passagem.updateView');
    Route::post('/update', 'PassagemController@update')->name('passagem.update');
    Route::get('/{id}/detalhe', 'PassagemController@detailed'); 
    Route::get('/{id}/destroy', 'PassagemController@destroy')->name('passagem.destroy');

    //Ramires 20/01
    Route::get('/api/{id}/{portal}/{timep}/{porta}','apicontroller@ApiPassagem');
});

Route::group(array('prefix' => 'relatorio'), function()
{
    Route::get('/pausacreate', 'RelatoriosController@pausacreate')->name('relatorio.pausacreate'); 
    Route::post('/pausa', 'RelatoriosController@pausa')->name('relatorio.pausa');
    Route::post('/pausa1', 'RelatoriosController@pausa')->name('relatorio.pausa1');
    Route::get('/tags', 'RelatoriosController@tags')->name('relatorio.tags'); 
    Route::get('/anormalidadecreate', 'RelatoriosController@anormalidadecreate')->name('relatorio.anormalidadecreate'); 
    Route::post('/anormalidade', 'RelatoriosController@anormalidade')->name('relatorio.anormalidade');
});
}
else{
    return redirect('/');
}